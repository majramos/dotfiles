" 
" Vim Settings 
" 
" Made by Marco Ramos <majramos@gmail.com>
"


" show line numbers
set number


" always show current position
set ruler


" height of the command bar
set cmdheight=1


" use spaces instead of tabs
set expandtab


" Be smart when using tabs ;)
set smarttab


" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4


" Linebreak on 500 characters
set lbr
set tw=500

set ai   "Auto indent
set si   "Smart indent
set wrap "Wrap lines


" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm


" Ignore case when searching
set ignorecase


" When searching try to be smart about cases
set smartcase


" Highlight search results
set hlsearch


" Makes search act like search in modern browsers
set incsearch


" Don't redraw while executing macros (good performance config)
set lazyredraw


" For regular expressions turn magic on
set magic


" Show matching brackets when text indicator is over them
set showmatch

" true color support
set termguicolors
" set colorscheme to solarized dark
syntax enable
set background=dark
try
    colorscheme solarized
catch
endtry

" Define status line
" Always show the status line
set laststatus=2

set statusline=
set statusline+=%1*\ %<%.40F%m%r%h%w\             " File path, modified, readonly, helpfile, preview
set statusline+=%2*\ %Y                           " FileType
set statusline+=%=                                " switch to right
set statusline+=\ %{''.(&fenc!=''?&fenc:&enc).''} " Encoding
set statusline+=\ (%{&ff})\                       " FileFormat (dos/unix..)
set statusline+=%3*\ col:\ %-2v\                  " Column number
set statusline+=\ln:\ %3l\/\%-3L\                 " Line number , total line 

hi User1 ctermfg=255 ctermbg=030 guibg=#008787 guifg=#eeeeee
hi User2 ctermfg=250 ctermbg=023 guibg=#005f5f guifg=#bcbcbc
hi User3 ctermfg=255 ctermbg=066 guibg=#eeeeee guifg=#bcbcbC


" Key Bindings
" map CTRL-S to save file
nnoremap <C-s> :w<CR>
" map CTRL-Q to exit
nnoremap <C-q> :q<CR>
" map CTRL-C to copy
inoremap <C-c> <Esc>"+yi
" map CTRL-V to paste
inoremap <C-v> <Esc>"+pi
" CTRL-T is new tab
nnoremap <C-t> :tabnew<CR>
" CTRL-Tab is next tab
nnoremap <C-tab> :tabnext<CR>
" CTRL-SHIFT-Tab is previous tab
nnoremap <C-S-tab> :tabprevious<CR>


