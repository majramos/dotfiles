#!/bin/bash
#
# Setups same bar in all monitors
# This is desinged for laptop, systray should be in main screen (eDP-1)

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch bar on all the monitors available
for m in $(polybar --list-monitors | cut -d":" -f1); do
    export MONITOR=$m
	if [[ $m == "eDP-1" ]]; then
        polybar --reload main &
        polybar --reload mainbottom &
        $HOME/.config/polybar/scripts/hideit.sh --wait --name '^polybar-mainbottom_eDP-1$' --direction bottom --region 0x1080+1920+-50 > /dev/null 2>&1 &
        $HOME/.config/polybar/scripts/hideit.sh --wait --name '^Polybar tray window$' --direction bottom --region 0x1080+1920+-50 > /dev/null 2>&1 &
    else
        polybar --reload secondary &
    fi
done
