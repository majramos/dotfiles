#!/bin/bash
#
# chmod u+x .config/polybar/scripts/update.sh 

# check for updates but catch output
update=$(dnf check-update -q 2>&1)

# DNF exit code will be 100 when there are updates available and a list of the updates will be printed
# 0 if no updates available
# 1 if an error occurs
exitcode=$?

if [[ $exitcode -eq 0 ]] || [[ $exitcode -eq 100 ]]; then

    count=$(echo "$update" | grep -v -e '^$' | wc -l)

    if ! [[ $count =~ ^[0-9]+$ ]]; then
	    color="#dc322f"
	    icon=""
	    count=""
    else
	    if (( $count == 0 )); then
		    color="#2aa198" # cyan
		    icon=""
		    count=""
	    elif (( $count > 0 )) && (( $count <= 15 )); then
		    color="#859900" # green
		    icon=""
	    elif (( $count > 15 )) && (( $count <= 30 )); then
		    color="#b58900" # yellow
		    icon=""
	    elif (( $count > 30 )) && (( $count <= 40 )); then
		    color="#cb4b16" # orange
		    icon=""
	    elif (( $count > 40 )); then
		    color="#dc322f" # red
		    icon=""
	    fi
    fi
else
    # in case of no network connection
    color="#b58900"
    icon=""
    count=""
fi

echo "%{F${color}}%{T4}${icon}%{T-}%{T2} $count%{T-}%{F-}"

