#!/bin/bash
#
# chmod u+x .config/polybar/scripts/redshift.sh 
# $(ps aux | grep redshift | grep S | grep -v grep| grep -v polybar/scripts | wc -l)

# check if any redshift process is running
check_if_running() {
	if [ $(systemctl --user is-active redshift) == "active" ]; then
		return 0
	else
		return 1
	fi
}

mode_toggle() {
  if check_if_running ; then
    systemctl --user stop redshift
  else
    systemctl --user start redshift
  fi
}

# Check if Day/Transition/Night
period=$(redshift -p 2> /dev/null | grep Period | awk '{print $2}')

if [[ $period == "Daytime" ]]; then
	periodo_icon=""
elif [[ $period == "Transition" ]]; then
	periodo_icon="嗀"
elif [[ $period == "Night" ]]; then
	periodo_icon="望"
else
	periodo_icon="${period:0:1}"
fi

run_check() {
	if check_if_running; then
		# maybe add this as icon 﨎
		echo "%{F#2aa198}%{T4}$periodo_icon%{T-}%{F-}"
	else
		echo "%{T4}$periodo_icon%{T-}"
	fi
}

case $1 in 
	toggle)
		mode_toggle
		run_check
		;;
	check)
		run_check
		;;
esac
