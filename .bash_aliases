
# alias

alias ls='ls -ahoF --group-directories-first --color=always'

alias rm='rm -iv'
alias cp='cp -iv'
alias mv='mv -iv'

# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -pv'

alias ..='cd ..'

# for .dofiles repo
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# use ranger as a nav tool
alias nav=ranger-cd

