# My Fedora setup
This is a collection of code to run when setting up my fedora+xfce4
setup (or any other distro, may require some adaptions). The code ir
supposed to be run in the presented order.


## Remove other instalations from boot (if necessary)
```bash
sudo rm -rf /boot/efi/EFI/<ubuntu>
```

## Add the necessary keyboard layouts
localectl list-keymaps:
- portuguse: pt-latin1
- english-international: us-intl
```bash
xfconf-query -c keyboard-layout -p /Default/XkbLayout -t 'string' -s 'us,pt'
xfconf-query -c keyboard-layout -p /Default/XkbVariant -t 'string' -s 'intl,'
```

## Update the hostname<sup>[1][1]</sup>
check for hostname
```bash
hostnamectl # or hostname
```
set hostname
```bash
hostnamectl set-hostname fedora-asus
```

## Firewall<sup>[2][2]</sup> (before connecting to internet)
check if firewalld is running
```bash
$ sudo firewall-cmd --state
running
```
set to log denied access
```bash
sudo firewall-cmd --set-log-denied=all
```
verify if is working
```bash
$ sudo firewall-cmd --get-log-denied
all
```
view denied packets
```bash
journalctl -x -e
```
create a file to store firewall logs
```bash
sudo vim /etc/rsyslog.d/firewalld-droppd.conf
```
add this to file above
```
:msg,contains,"_DROP" /var/log/firewalld-droppd.log
:msg,contains,"_REJECT" /var/log/firewalld-droppd.log
& stop
```

restart service
```bash
sudo systemctl restart rsyslog.service
```
check for logs in the file created previously
```bash
sudo tail -f /var/log/firewalld-droppd.log
```

## Packages update <sup>[3][3]</sup>
change dnf settings first
```bash
sudo vim /etc/dnf/dnf.conf
```
add lines
```
max_parallel_downloads=10
defaultyes=True
```

check for packages with updates available
```bash
dnf check-update
```
summary of updates available / which type they belong to
```bash
dnf updateinfo # list
```
start update
```bash
sudo dnf upgrade
```
update just for security
```bash
sudo dnf upgrade --security
```
perform a reboot to complete the Fedora update
```bash
sudo shutdown -r now
```
clean up, remove obsolete packages from disk (optional)
```bash
sudo dnf clean packages
```
disable dnfdragora (update with dnf)
```bash
cp /etc/xdg/autostart/org.mageia.dnfdragora-updater.desktop $HOME/.config/autostart
```
edit file to stop starting
```bash
vim $HOME/.config/autostart/org.mageia.dnfdragora-updater.desktop
```
add  to end of file above
> Hidden=true

## Add RPM fusion repositories and flatpack hub<sup>[4][4]</sup>
install RPM fusion free repository and NON-free repository
```bash
$ sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
$ sudo dnf install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```
update dnf package repository
```bash
sudo dnf makecache
```
add flatpack repo
```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## Need to add aditional stuff to .bashrc
```bash
# Alias definitions
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Custom prompt
if [ -f ~/.prompt ]; then
    . ~/.prompt
fi
```

## Setup git and configuration/dot files<sup>[5][5]</sup>
make sure git is installed
```bash
sudo dnf install git
```
set default branch name main
```bash
git config --global init.defaultBranch main
```
#### Setup .dotfiles repo and configuration<sup>[6][6] [7][7]</sup>
clone dotfiles (use https for now)
```bash
git clone --bare https://gitlab.com/majramos/dotfiles.git $HOME/.dotfiles
```
set temp alias config
```bash
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```
get content form bare to $HOME
```bash
config checkout
```
set to not show untracked files
```bash
config config --local status.showUntrackedFiles no
```
#### Setup ssh key for git<sup>[8][8]</sup>
name it "id_ed25519_gitlab"
```bash
ssh-keygen -t ed25519 -C "majramos@gmail.com"
```
add id_ed25519_gitlab.pub to gitlab web
**check that git local git repos are referencing the ssh url**
**reset permission on ~/.ssh/config after cloning**
```bash
chmod go-w ~/.ssh/config
```
push current branch and set remote as upstream (to start tracking remote)
```bash
config push --set-upstream origin main
```

## Install Nvidia drivers<sup>[9][9]</sup>
**Required to update dnf first and add RPM Fusion repos first**

check installed gpus
```bash
lspci | egrep 'VGA|NVIDIA'
```
update system and reboot if needed
```bash
sudo dnf upgrade
```

#### Secure Boot and signing NVIDIA drivers with MOK<sup>[10][10]</sup>
**Do this first!**
process described in the following README
> /usr/share/doc/akmods/README.secureboot

Install the tools required for auto signing to work
```bash
sudo dnf install kmodtool akmods mokutil openssl
```
Generate a signing key
```bash
sudo kmodgenca -a
```
Initiate the key enrollment - This will make Linux kernel trust drivers signed with your key
```bash
sudo mokutil --import /etc/pki/akmods/certs/public_key.der
```
**You will be asked to enter a password, it doesn’t have to be very strong, just make sure to remember it.**
Reboot to enroll the key
```bash
sudo reboot
```
Enroll the key - MOK manager
```
1. select “Enroll MOK“
2. select "Continue"
3. select "Yes"
4. enter password
5. select "OK"
```

#### Install NVIDIA driver
```bash
sudo dnf install gcc kernel-headers kernel-devel akmod-nvidia xorg-x11-drv-nvidia xorg-x11-drv-nvidia-libs xorg-x11-drv-nvidia-libs.i686
```
Make sure the kernel modules got compiled
```bash
sudo akmods --force
```
Make sure the boot image got updated as well
```bash
sudo dracut --force
```
Reboot your device
```bash
sudo reboot
```
check that you are using nvidia drivers
```bash
lsmod | grep -i nvidia
```

## Install required packages/apps
htop - process viewer - https://htop.dev/
```bash
sudo dnf install htop
```
neofetch - system information tool - https://github.com/dylanaraps/neofetch
```bash
sudo dnf install neofetch
```
timeshift - system restore tool - https://github.com/teejee2008/timeshift
```bash
sudo dnf install timeshift
```
ranger - console file manager - https://github.com/ranger/ranger
```bash
sudo dnf install ranger
```
rofi - application launcher - https://github.com/davatorium/rofi
```bash
sudo dnf install rofi
```
kitty - terminal - https://github.com/kovidgoyal/kitty
```bash
sudo dnf install kitty
```
redshift - color temperature - https://github.com/jonls/redshift
```bash
sudo dnf install redshift
```
slop - queries for a selection - https://github.com/naelstrof/slop
```bash
sudo dnf install slop
```
podman - deamonless container engine - https://podman.io
```bash
sudo dnf install podman
```

## Setup wallpaper
asus vivobook monitors (monitor name)
- main: monitoreDP-1
- hdmi: monitorHDMI-2
list monitors
```bash
xfconf-query -c xfce4-desktop -p /backdrop -lv
```
set image
```bash
xfconf-query -c xfce4-desktop -p  /backdrop/screen0/<monitor name>/workspace0/last-image -s $HOME/Pictures/oram_blue_moon.jpg
```
set image setting as zoomed
```bash
xfconf-query -c xfce4-desktop -p  /backdrop/screen0/<monitor name>/workspace0/image-style -s 5
```

## Setup font

Create a new directory ~/.local/share/fonts/<font-family-name>/ for the new font family
```bash
mkdir -p $HOME/.local/share/fonts/cascadiacode
```
Copy font files (e.g. .ttf files) to the new directory
```bash
cp ~/Downloads/'Caskaydia Cove Nerd Font Complete Mono.ttf' ~/.local/share/fonts/cascadiacode/
```
Update the font cache
```bash
fc-cache -v
```
Get real name of the font
```bash
$ fc-list | grep mononoki
$HOME/.local/share/fonts/mononoki/mononoki-Regular Nerd Font Complete Mono.ttf:mononoki Nerd Font Mono:style=Regular
```
set the font to the system
```bash
xfconf-query -c xsettings -p '/Gtk/FontName' -s 'mononoki Nerd Font 10'
xfconf-query -c xsettings -p '/Gtk/MonospaceFontName' -s 'mononoki Nerd Font Mono 10'
```

## Install conky - light-weight system monitor<sup>[11][11]</sup>
```bash
sudo dnf install conky
```
or download latest AppImage https://github.com/brndnmtthws/conky/releases/latest
```bash
mv Downloads/conky-x86_64.AppImage Applications/conky-x86_64.AppImage
chmod +x $HOME/Applications/conky-x86_64.AppImage
```
to run the image
```bash
$HOME/Applications/conky-x86_64.AppImage
```

## Setup shortcuts
list commands and respective actions
```bash
xfconf-query -c xfce4-keyboard-shortcuts -lv
```
or lookup in
```bash
vim ~/.config/xfce4/xfconf/xfce-perchannel-xml
```

#### Common start up commands, always between <>
- Shift
- Alt
- Super
- Primary = Ctrl)
- space
- KP_ stands for Keypad - to be used without Numlock

custom command shortcuts will be placed in */commands/custom/...*

custom window manager shortcuts will be placed in */xfwm4/custom/...*

#### how to set a shortcut
```bash
xfconf-query
    \ -c xfce4-keyboard-shortcuts  # select channel
    \ -n  # set a New command
    \ -t 'string'  # has to be Type string
    \ -p '/commands/custom/<Primary><Alt>t'  # the Property or commmand to change
    \ -s xfce4-terminal  # Set the value for the property
```
#### app shortcuts
```bash
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/commands/custom/<Super>Return' -s kitty
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/commands/custom/<Super>space' -s 'rofi -show run'
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/commands/custom/<Super>w' -s 'exo-open --launch WebBrowser'
```
#### window manager shortcuts
```bash
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Super>q' -s close_window_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Super>Tab' -s next_workspace_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Primary>Tab' -s prev_workspace_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Primary>Up' -s tile_up_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Primary>Down' -s tile_down_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Primary>Left' -s tile_left_key
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/xfwm4/custom/<Primaryq>Right' -s tile_right_key
```
#### set to raise windows while cycling
```bash
xfconf-query -c xfwm4 -p /general/cycle_minimized -s true
xfconf-query -c xfwm4 -p /general/cycle_raise -s true
```
#### workspaces
```bash
xfconf-query -c xfwm4 -p /general/workspace_count -s 2
```
#### power manager
```bash
xfconf-query -c xfce4-power-manager  -p /xfce4-power-manager/blank-on-ac -s 0
```
switch off after
```bash
xfconf-query -c xfce4-power-manager  -p /xfce4-power-manager/dpms-on-ac-off -s 0
```
put to sleep after
```bash
xfconf-query -c xfce4-power-manager  -p /xfce4-power-manager/dpms-on-ac-sleep -s 0
```

## Setup grub<sup>[12][12]</sup>
Download theme from here:
- https://github.com/AdisonCavani/distro-grub-themes/releases ... fedora.tar
save it in ~/.config/grub
```bash
mv $HOME/Downloads $HOME/.config/grub
```
make folder for storage
```bash
mkdir $HOME/.config/grub/fedora
```
unpack tar
```bash
tar -xvf $HOME/.config/grub/fedora.tar -C $HOME/.config/grub/fedora
```
make folder in boot folder
```bash
sudo mkdir /boot/grub2/themes
```
copy theme to boot folder
```bash
sudo cp -r $HOME/.config/grub/fedora/ /boot/grub2/themes
```
edit grub config
```bash
sudo vim /etc/default/grub
```
change these lines in the file above
```
comment out: GRUB_TERMINAL_OUTPUT="console"
uncomment or add: GRUB_GFXMODE="1920x1080x32"
add GRUB_THEME="/boot/grub2/themes/fedora/theme.txt"
```
update grub config on fedora
```bash
sudo grub2-mkconfig -o /etc/grub2.cfg && sudo grub2-mkconfig -o /etc/grub2-efi.cfg && sudo grub2-mkconfig /etc/grub2-efi.cfg
```

## Setup cursor<sup>[13][13]</sup>
download theme from one of the options below
- https://www.xfce-look.org/p/1405210
- https://gitlab.com/zoli111/simp1e/
make folder to store icons
```bash
mkdir $HOME/.icons
```
move download to folder
```bash
mv $HOME/Downloads/Simp1e-Solarized-Dark.tgz $HOME/.icons/Simp1e-Solarized-Dark.tgz
```
unpack files
```bash
tar -xvf $HOME/.icons/Simp1e-Solarized-Dark.tgz -C $HOME/.icons/
```
remove unecessary files
```bash
rm -f $HOME/.icons/how_to_install.txt $HOME/.icons/Simp1e-Solarized-Dark.tgz
```
set theme
```bash
xfconf-query -c xsettings -p '/Gtk/CursorThemeName' -s 'Simp1e-Solarized-Dark'
```

## Setup icon theme<sup>[14][14]</sup>
download theme from one of the options below
- https://www.pling.com/p/1279924/
- https://github.com/vinceliuice/Tela-icon-theme
make folder to store icons if not existing
```bash
mkdir $HOME/.icons
```
move download to .icons folder
```bash
mv $HOME/Downloads/Tela-manjaro.tar.xz $HOME/.icons/Tela-manjaro.tar.xz
```
unpack files
```bash
tar -xvf $HOME/.icons/Tela-manjaro.tar.xz -C $HOME/.icons/
```
remove unecessary files
```bash
rm -f $HOME/.icons/Tela-manjaro.tar.xz
```
set theme
```bash
xfconf-query -c xsettings -p '/Net/IconThemeName' -s 'Tela-manjaro-dark'
```

## Setup style theme
download theme from one of the options below
- https://www.gnome-look.org/p/1308808/
make folder to store icons if not existing
```bash
mkdir $HOME/.themes
```
move download to .icons folder
```bash
mv $HOME/Downloads/Solarized-Dark-Cyan-3.36_2.0.4.zip $HOME/.themes/Solarized-Dark-Cyan-3.36_2.0.4.zip
```
unpack files
```bash
unzip $HOME/.themes/Solarized-Dark-Cyan-3.36_2.0.4.zip -d $HOME/.themes/
```
remove unecessary files
```bash
rm -f $HOME/.themes/Solarized-Dark-Cyan-3.36_2.0.4.zip
```
set theme
```bash
xfconf-query -c xsettings -p '/Net/ThemeName' -s 'Solarized-Dark-Cyan-3.36'
```
set theme for window manager
```bash
xfconf-query -c xfwm4 -p '/general/theme' -s 'Solarized-Dark-Cyan-3.36'
```

## Setup display manager (lightdm)
check which display manger
```bash
cat /etc/sysconfig/desktop
```
check available greeters
```bash
ls /usr/share/xgreeters/*.desktop
```
files for editing lightdm (need sudo for editing)
```bash
vim /etc/lightdm/lightdm.conf
vim /etc/lightdm/lightdm-gtk-greeter.conf
```
move background image/theme/icon to the necessary location
```bash
sudo cp $HOME/Pictures/oram_blue_moon.jpg /usr/share/backgrounds/oram_blue_moon.jpg
sudo cp -r $HOME/.themes/Solarized-Dark-Cyan-3.36 /usr/share/themes/Solarized-Dark-Cyan-3.36
sudo cp -r $HOME/.icons/Simp1e-Solarized-Dark /usr/share/icons/Simp1e-Solarized-Dark
sudo cp -r $HOME/.icons/Tela-manjaro-dark /usr/share/icons/Tela-manjaro-dark
```
change settings in
```bash
sudo vim /etc/lightdm/lightdm-gtk-greeter.conf
```
add this to lightdm-gtk-greeter.conf file
```
[greeter]
background=/usr/share/backgrounds/oram_blue_moon.jpg
theme-name=Solarized-Dark-Cyan-3.36
icon-theme-name=Tela-manjaro-dark
cursor-theme-name=Simp1e-Solarized-Dark
```

## Setup plymouth<sup>[15][15]</sup>
fedora graphical bootup sequence

list available themes
```bash
plymouth-set-default-theme --list
```
install theme
```bash
sudo dnf install plymouth-theme-spinfinity
```
set theme
```bash
sudo plymouth-set-default-theme spinfinity -R
```

## install mulimedia codecs<sup>[16][16]</sup>
```bash
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
sudo dnf groupupdate sound-and-video
```

## Setup polybar<sup>[17][17]</sup>
installation
```bash
sudo dnf install polybar
```
make folder for configuration if necessary
```bash
mkdir $HOME/.config/polybar
```
copy base configuration if necessary
```bash
cp /etc/polybar/config.ini $HOME/.config/polybar/config.ini
```
#### add to autostart
```bash
vim $HOME/.config/polybar/launch.sh
```
make it executable
```bash
chmod +x $HOME/.config/polybar/launch.sh
```
#### remove xfce panel form start up
make the file editable
```bash
sudo chmod +r /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
```
open file to edit
```bash
sudo vim /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
```
> coment out <!-- --> property with value="xfce4-panel"

#### Enable changing the backlight with the scroll wheel<sup>[18][18]</sup>
The recommended way is to add the user to the `video` group and give
that group write-privileges for the `brightness` file

create rules in */etc/udev/rules.d/backlight.rules*
```
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/usr/bin/chgrp video /sys/class/backlight/intel_backlight/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/usr/bin/chmod g+w /sys/class/backlight/intel_backlight/brightness"
``

add user to group video (the logout and logon)
```bash
sudo usermod -aG video $USER
```
make necessary scripts executable
```bash
chmod +x .config/polybar/scripts/windows.sh
```

#### make bottom bar hide
install required packages
```bash
sudo dnf install xdotool xwininfo xev
```
get bar windows name
```bash
$ xprop | grep WM_NAME
WM_NAME(STRING) = "polybar-mainbottom_eDP-1"
```
run script
```bash
$HOME/.config/polybar/scripts/hideit.sh --wait --name '^polybar-mainbottom_eDP-1$' --direction bottom --region 0x1080+1920+-50 > /dev/null 2>&1 &

$HOME/.config/polybar/scripts/hideit.sh --wait --name '^Polybar tray window$' --direction bottom --region 0x1080+1920+-50 > /dev/null 2>&1 &
```

# Other References
[Vim theme setup](https://github.com/ericbn/vim-solarized)
[polywins](https://github.com/tam-carre/polywins)
[NVIDIA drivers 1](https://linuxhint.com/install-nvidia-drivers-on-fedora-35)
[NVIDIA drivers 2](https://rpmfusion.org/Howto/Secure%20Boot)
[NVIDIA drivers 3](https://blog.monosoul.dev/2022/05/17/automatically-sign-nvidia-kernel-module-in-fedora-36/)

[1]: https://www.cyberciti.biz/faq/howto-change-hostname-in-fedora-linux-permanently/ "https://www.cyberciti.biz/faq/howto-change-hostname-in-fedora-linux-permanently/"
[2]: https://bobcares.com/blog/enable-firewalld-logging-for-denied-packets-on-linux "https://bobcares.com/blog/enable-firewalld-logging-for-denied-packets-on-linux"
[3]: https://linoxide.com/update-fedora-linux-to-get-latest-software/ "https://linoxide.com/update-fedora-linux-to-get-latest-software/"
[4]: https://rpmfusion.org/ "https://rpmfusion.org/"
[5]: https://git-scm.com/download/linux "https://git-scm.com/download/linux"
[6]: https://www.atlassian.com/git/tutorials/dotfiles "https://www.atlassian.com/git/tutorials/dotfiles"
[7]: https://www.adamdehaven.com/blog/using-dotfiles-and-git-to-manage-your-development-environment-across-multiple-computers "https://www.adamdehaven.com/blog/using-dotfiles-and-git-to-manage-your-development-environment-across-multiple-computers"
[8]: https://docs.gitlab.com/ee/user/ssh.html "https://docs.gitlab.com/ee/user/ssh.html"
[9]: https://linuxhint.com/install-nvidia-drivers-on-fedora-35/ "https://linuxhint.com/install-nvidia-drivers-on-fedora-35/"
[10]: https://src.fedoraproject.org/rpms/akmods/tree/rawhide "https://src.fedoraproject.org/rpms/akmods/tree/rawhide"
[11]: https://github.com/brndnmtthws/conky "https://github.com/brndnmtthws/conky"
[12]: https://github.com/AdisonCavani/distro-grub-themes "https://github.com/AdisonCavani/distro-grub-themes"
[13]: https://gitlab.com/zoli111/simp1e/ "https://gitlab.com/zoli111/simp1e/"
[14]: https://github.com/vinceliuice/Tela-icon-theme "https://github.com/vinceliuice/Tela-icon-theme"
[15]: https://fedoramagazine.org/howto-change-the-plymouth-theme/ "https://fedoramagazine.org/howto-change-the-plymouth-theme/"
[16]: https://rpmfusion.org/Howto/Multimedia "https://rpmfusion.org/Howto/Multimedia"
[17]: https://github.com/polybar/polybar "https://github.com/polybar/polybar"
[18]: https://wiki.archlinux.org/title/Backlight#ACPI "https://wiki.archlinux.org/title/Backlight#ACPI"
